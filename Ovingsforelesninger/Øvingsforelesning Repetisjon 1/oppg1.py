# Lag en funksjon som tar inn to tall som parametre,
# og returnerer produktet av tallene.

def produkt(tall1, tall2):
    return tall1 * tall2

print(produkt(3,5))   # Eksempel på kjøring
    