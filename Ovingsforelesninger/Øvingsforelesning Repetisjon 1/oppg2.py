# Lag en funksjon som tar inn et tall som parameter
# og returnerer absoluttverdien av et tallet.

def absoluttverdi(tall):
    if tall >= 0:
        return tall
    else:
        return -tall
    
print(absoluttverdi(-14))   # Eksempel på kjøring
