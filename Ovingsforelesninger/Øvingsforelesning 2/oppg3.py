# Lag et program som tar inn et tall og sjekker om det er et partall.

tall = int(input("Skriv inn et tall: "))

er_partall = (tall % 2 == 0)

print("Er", tall, "et partall?", er_partall)