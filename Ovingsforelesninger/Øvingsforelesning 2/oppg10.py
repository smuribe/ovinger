# Lag et program som tar inn en tekststreng fra brukeren.
# Ut i fra strengen kan følgende fire ting skje:

# 1) Dersom strengen er lik ”IalwaysCheat” skal programmet skrive ut
#    ”Juksing er ikke lov, prøv igjen senere” 
# 2) Ellers, dersom lengden på strengen er større enn 4 og mindre enn 10
#    skal programmet skrive ut ”Dette var en streng med perfekt lengde”. 
#    Hint: len(streng) gir lengden på strengen med navn ”streng”. 
# 3) Ellers, hvis strengen starter på ”hei” og har mer enn 6 tegn
#    skal programmet skrive ut ”Hei på deg også”
#    Hint: streng.startswith("hei") gir True hvis strengen starter med ”hei” 
# 4) Ellers skal programmet skrive ut ”Dette var en kjedelig streng”

streng = input("Skriv noe: ")
lengde = len(streng)

if streng == "Ialwayscheat":
    print("Juksing er ikke lov, prøv igjen senere")
elif (lengde > 4 and lengde < 10):
    print("Dette var en streng med perfekt lengde.")
elif (streng.startswith("hei") and lengde > 6):
    print("Hei på deg også!")
else:
    print("Dette var en kjedelig streng.")
