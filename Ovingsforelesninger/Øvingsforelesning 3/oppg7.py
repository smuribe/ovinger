# Lag et program som gjør følgende
# Grønn: 
# 1) Tar inn et tall fra brukeren
# 2) Sjekker om tallet er likt et annet tall du har bestemt på forhånd
# 3) Hvis tallene ikke er like: si om det innskrevne tallet er større
#    eller mindre enn målet
# Gul:
# 4) La brukeren gjette tall igjen
# Rød: 
# 5) Skriv at tallet er mye større / mindre hvis det er mer enn 10 i differanse

gjett = int(input("Gjett et tall: "))
fasit = 33

diff = abs(gjett - fasit)

if gjett > fasit and diff > 10:
    print("Tallet du gjettet er altfor høyt")
elif gjett > fasit:    
    print("Tallet du gjettet er for høyt")
elif gjett < fasit and diff > 10:
    print("Tallet du gjettet er altfor lavt")
elif gjett < fasit:
    print("Tallet du gjettet er for lavt")
    
if gjett != fasit:
    gjett = int(input("Gjett på nytt: "))
